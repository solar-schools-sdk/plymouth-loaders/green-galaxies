.PHONY: install uninstall set-default

uninstall:
	rm -rv /usr/share/plymouth/themes/green-galaxies || true

install: uninstall
	mkdir /usr/share/plymouth/themes/green-galaxies
	cp -v *.script *.plymouth *.png /usr/share/plymouth/themes/green-galaxies/

set-default:
	plymouth-set-default-theme -R green-galaxies
