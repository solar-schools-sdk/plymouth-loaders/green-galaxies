### Installation

    wget https://gitlab.com/solarschools/plymouth-loaders/green-galaxies/-/archive/master/green-galaxies-master.tar.gz
    tar xf green-galaxies-master.tar.gz
    cd green-galaxies-master
    sudo make install   # as root or with sudo
    cd ..
    rm -rf green-galaxies-master* 
    sudo plymouth-set-default-theme green-galaxies
    # reboot to see the new theme

### License

This theme is licensed under GPLv2, for more details check COPYING.
