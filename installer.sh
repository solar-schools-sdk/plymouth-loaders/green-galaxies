#!/bin/sh
wget https://gitlab.com/solarschools/plymouth-loaders/solar-schools/-/archive/master/solar-schools-master.tar.gz
tar xf solar-schools-master.tar.gz
cd solar-schools-master
sudo make install
cd ..
rm -rf solar-schools-master* 
sudo plymouth-set-default-theme -R solar-schools
